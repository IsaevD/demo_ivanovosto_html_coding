function initializePopupContainer(object, link, type) {

    initializeOpenPopupContainer();
    initializeClosePopupContainer();

    function initializeOpenPopupContainer() {
        $("body").on("click", link, function(){
            $(".popup").show();
            $(object).show();

            if(type == "image") {
                fullsize_img = $(object).children("img");
                small_img = $(this).children("img");
                $(fullsize_img).attr("src", $(small_img).data("url"));
                $(object).children(".description").text($(this).children(".description").text());
            }
        });
    }

    function initializeClosePopupContainer() {
        $(".popup .container").click(function(e) {
            e.stopPropagation();
        });
        $(object).children(".cross").click(function(){
            $(".popup").click();
        });
        $(".popup").click(function(){
            $(this).hide();
            $(object).hide();
        });
    }

}