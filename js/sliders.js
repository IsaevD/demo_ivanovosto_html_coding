// ������������� ��������������� �������� � ������������
function initializeImageSlider(object, left_btn, right_btn, autoscroll, is_paginate) {
    var intervalID = 0;
    var childrens = $(object.children(".values"));

    childrens.children(".value").first().addClass("active");
    if(is_paginate != null) {
        var paginator = $(object.children(".paginator"));
        paginator.children(".paginator_item").first().addClass("active");
    }

    reinitializeChildrenWidth();

    // ������������ �����
    if(left_btn != null) {
        $(left_btn).click(function () {
            previousSlide();
        });
    }
    // ������������ ������
    if(right_btn != null) {
        $(right_btn).click(function () {
            nextSlide();
        });
    }

    if (autoscroll != null) {
        setAutoSwitch();
    }

    function nextSlide() {
        slideByAction("right");
    }
    function previousSlide() {
        slideByAction("left");
    }

    if(is_paginate != null) {
        $("body").on("click", ".paginator_item", function(){
            if(!$(this).hasClass("no_item")) {
                slideByIndex($(this).index());
                return false;
            }
        });
    }

    function slideByAction(type) {
        var current = $($(childrens).children(".value.active")).index();
        var length = $(childrens).children(".value").length - 1;
        if (length > 2) {
            switch (type) {
                case "right":
                    if (current == length)
                        current = 0;
                    else
                        current = current + 1;
                    break;
                case "left":
                    if (current == 0)
                        current = length;
                    else
                        current = current - 1;
                    break;
            }
            switchSlide(current);
        }
    }

    function slideByIndex(index) {
        var length = $(childrens).children(".value").length - 1;
        if ((index < 0) || (index > length))
            slideByIndex(0);
        else {
            switchSlide(index);
        }
    }

    function switchSlide(index) {
        var width = $(childrens).children(".value.active").width();
        $(childrens).animate({left: -( index * width )}, 1000);
        $($(childrens).children(".value")).removeClass("active");
        $($(childrens).children(".value")[index]).addClass("active");
        if(is_paginate != null) {
            $($(paginator).children(".paginator_item")).removeClass("active");
            $($(paginator).children(".paginator_item")[index]).addClass("active");
        }
    }

    function setAutoSwitch() {
        clearInterval(intervalID);
        intervalID = setInterval(function(){ nextSlide(); }, 5000);
    }

    function reinitializeChildrenWidth() {
        var width = $(childrens).children(".value.active").width();
        var current = $($(childrens).children(".value.active")).index();
        $(childrens).children("li").width(object.width());
        $(childrens).css("left", -( current * width ));
    }

    $(window).resize(function(){
        reinitializeChildrenWidth();
    });

}