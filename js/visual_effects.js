function initializeSyncShowHideEffect(showed_object, hidden_object) {

    $(showed_object).click(function(){
        changeObjectsState();
    });

    $(hidden_object).click(function(){
        changeObjectsState();
    });

    function changeObjectsState() {
        if ($(showed_object).is(":visible")) {
            $(showed_object).hide();
            $(hidden_object).show();
        } else {
            $(showed_object).show();
            $(hidden_object).hide();
        }
    }
}

function initializeAsyncShowHideEffect(showed_object, parent_first, parent_second) {

    $(parent_first).click(function(){
        changeObjectsState();
    });
    if($(parent_first).attr("class") != $(parent_second).attr("class")) {
        $(parent_second).click(function(){
            changeObjectsState();
        });
    }

    function changeObjectsState() {
        if ($(showed_object).is(":visible")) {
            $(showed_object).hide();
        } else {
            $(showed_object).show();
        }
    }
}