$(function(){

    $("#arrow_top").click(function(){
        $("body").animate({scrollTop:0}, '500', 'swing', null);
    });

    $(".popup form").submit(function(){
        $(this).hide();
        $(this).parent().children(".complete_flash").show();
        return false;
    });

    if($(".quote_cnt.activeShowHide").length != 0){
        initializeSyncShowHideEffect($(".quote_cnt.activeShowHide .quote"), $(".quote_cnt.activeShowHide .label"));
    }
    if($("#order_btn").length != 0){
        initializePopupContainer("#order_form", "#order_btn");
    }
    if($(".main_slider").length != 0) {
        initializeImageSlider($(".main_slider"), null, null, true, true);
    }
    if($(".news_slider").length != 0) {
        initializeImageSlider($(".news_slider"), null, null, null, true);
    }
    if($("#order_form").length != 0) {
        initializePopupContainer("#order_form", "#order_phone_btn");
    }
    if($("#request_form").length != 0) {
        initializePopupContainer("#request_form", "#order_request_btn");
    }
    if($(".inner_block .image").length != 0) {
        initializePopupContainer("#image_fullsize", ".inner_block .image", "image");
    }
    if($(".feedback_btn_cnt").length != 0) {
        initializeSyncShowHideEffect($(".feedback_btn"), $(".close_feedback_btn"));
        initializeAsyncShowHideEffect($("#feedback_form"), $(".feedback_btn"), $(".close_feedback_btn"));
    }

});